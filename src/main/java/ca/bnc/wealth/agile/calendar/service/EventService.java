package ca.bnc.wealth.agile.calendar.service;

import ca.bnc.wealth.agile.calendar.model.Event;
import ca.bnc.wealth.agile.calendar.repository.EventRepository;
import ca.bnc.wealth.agile.calendar.repository.InviteRepository;
import ca.bnc.wealth.agile.calendar.service.exception.UnknownEventException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;


import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class EventService {
    private final EventRepository eventRepository;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public Event getEvent(Long id) {
        Assert.notNull(id, "Event ID cannot be null");
        return eventRepository.findById(id)
                .orElseThrow(() -> new UnknownEventException(id));
    }

    public Event createEvent(Event event) {
        Assert.notNull(event, "Event cannot be null");
        final Event newEvent = new Event(
                    event.getTitle(),
                    event.getDescription(),
                    event.getStartDate(),
                    event.getEndDate(),
                    event.getLocation()
                );

        return eventRepository.save(newEvent);
    }

    public void  updateEvent(Event event) {
        Assert.notNull(event, "Event cannot be null");
        eventRepository.save(event);
    }

    public void deleteEvent(Long eventId) {
        Assert.notNull(eventId, "Event ID cannot be null");
        this.eventRepository.deleteById(eventId);
    }

    public List<Event> getEvents() {
        return  eventRepository.findAll();
    }

    public List<Event> getEvents(Date startDate, Date endDate) {
        return  eventRepository.findByStartDateAndEndDate(startDate, endDate);
    }

}
