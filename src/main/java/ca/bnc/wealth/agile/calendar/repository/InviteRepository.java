package ca.bnc.wealth.agile.calendar.repository;

import ca.bnc.wealth.agile.calendar.model.Invite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InviteRepository  extends JpaRepository<Invite, Long> {
}
