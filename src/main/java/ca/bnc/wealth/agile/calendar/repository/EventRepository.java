package ca.bnc.wealth.agile.calendar.repository;

import ca.bnc.wealth.agile.calendar.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

    @Query("SELECT e FROM Event e where e.startDate >= :startDate and e.endDate <= :endDate")
    List<Event> findByStartDateAndEndDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
