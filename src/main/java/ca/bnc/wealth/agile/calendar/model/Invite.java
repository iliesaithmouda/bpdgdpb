package ca.bnc.wealth.agile.calendar.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Email;
import java.util.Objects;

@Entity
public class Invite {
    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    @Email
    private String email;

    @ManyToOne()
    private Event event;

    public Invite() {
    }

    public Invite(Long id, @NotEmpty @Email String emai, Event eventl) {
        this.id = id;
        this.email = email;
        this.event = eventl;
    }

    @PersistenceConstructor
    public Invite(String email, Event event) {
        this.email = email;
        this.event = event;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invite invite = (Invite) o;
        return Objects.equals(id, invite.id) &&
                Objects.equals(email, invite.email) &&
                Objects.equals(event, invite.event);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, event);
    }
}
